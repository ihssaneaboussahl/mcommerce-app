const mongoose = require('mongoose');
require('dotenv').config();

mongoose.connect(process.env.MONGO_URL
, { useNewUrlParser: true, useUnifiedTopology: true })
.then(() => console.log(`Connected to: ${mongoose.connection.name}`))
.catch(err => console.log(err));



module.exports = mongoose;
